class Teacher {
    constructor(nameTeacher, specialityTeacher) {
        this.name = nameTeacher;
        this.speciality = specialityTeacher;
        this.hired = true;
    }
}