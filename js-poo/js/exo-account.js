class Account {
    constructor() {
        this.solde = 0.00;
        this.id = '';
        this.customerName = '';
        this.soldeLimit = 0.00;
    }
    withdraw(amount) {
        this.solde -= amount;
    }

    deposit(amount) {
        this.solde += amount;
    }

    transfert(Account, amount) {
        this.solde += amount;
        Account.solde -= amount;
    }

    debit(Account, amount) {
        this.solde -= amount;
        Account.solde -= amount;
    }
}