# js-poo

## Exo Counter
1. Dans le projet js-poo, créer un fichier exercise-counter.html et exercise-counter.js ainsi qu'un fichier counter.js
2. Dans le HTML, charger d'abord le fichier counter.js puis le fichier exercise-counter.js
3. Dans le fichier counter.js, créer une classe Counter qui aura dans son constructeur une propriété value initialisée à 0 par défaut
4. Dans exercise-counter.js faire une instance du counter et faire un console.log de sa value
5. Dans la classe Counter, rajouter une méthode increment() qui viendra ajouter 1 à la propriété value de la classe
Pour accéder aux propriétés d'une classe, on garde en tête que c'est this.propriete et à partir de là, ça s'utilise comme une variable classique
(On en profite pour tester dans le exercise-counter.js si la méthode marche bien en l'appelant 1 ou 2 fois sur notre instance avant le console log)
6. Faire une seconde méthode decrement() qui fait pareil, mais qui retire 1 à la propriété value puis la méthode reset() qui met cette propriété à zéro
7. Faire une deuxième instance et via des consoles log (ou directement dans la console du navigateur) voir comment se comporte les compteurs l'un par rapport à l'autre (genre si je déclenche le increment() chez l'un, est-ce que ça change la value de l'autre ?)

![diagramme classe counter](./uml/counter.png)

## Exo Todo
### I. La classe Task
1. Créer un fichier exercise-todo.html, exercise-todo.js et task.js
2. Dans task.js, créer une classe Task qui aura une propriété label en string et une propriété done en booléen initialisée à false
3. Ajouter dans la classe Task une méthode toggleDone() qui passera la propriété done de true à false ou de false à true
4. Dans exercise-todo.js faire deux ou trois instances de Task et tester la méthode toggleDone pour voir si elle marche bien

![diagramme classe task](./uml/task.png)

### II. La classe TodoList
1. Créer un nouveau fichier todo-list.js et le charger dans le html (entre task et exercise-todo)
2. Dans ce fichier, créer une classe TodoList qui aura une propriété tasks qui sera initialisée comme un tableau vide
3. Rajouter dans la classe TodoList une méthode addTask qui attendra du string en argument, cette méthode va créer une instance de Task et la push dans la propriété tasks de TodoList
en gros on fait une méthode addTask(text) et dans la méthode on fait un new Task en lui donnant la variable text en argument, puis on fait un push de cette Task sur la propriété this.tasks
4. Faire la méthode clearDone() dans TodoList qui va supprimer de la propriété tasks toutes les tâches qui ont leur done à true (plus d'infos en spoiler)
plusieurs manières possibles, la plus simple est d'utiliser la méthode filter(), chercher Array.filter sur mozilla par exemple pour voir comment l'utiliser. Alternativement, on peut faire une boucle for classique qui parcours this.tasks à l'envers (en partant de la fin pour aller jusqu'à zéro) et dans la boucle, faire un splice si la tâche actuelle a done === true

![diagramme classe task](./uml/todo-list.png)
